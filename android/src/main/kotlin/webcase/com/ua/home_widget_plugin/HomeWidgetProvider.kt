package webcase.com.ua.home_widget_plugin

import android.appwidget.AppWidgetManager
import android.appwidget.AppWidgetProvider
import android.content.Context
import android.widget.RemoteViews
import org.json.JSONObject

abstract class HomeWidgetProvider : AppWidgetProvider() {

    override fun onUpdate(context: Context, appWidgetManager: AppWidgetManager, appWidgetIds: IntArray) {
        super.onUpdate(context, appWidgetManager, appWidgetIds)

        val views = RemoteViews(context.packageName, getRemoteViewsLayout())
        appWidgetIds.forEach { appWidgetId ->
            val sp = HomeWidgetPlugin.getData(context)
            val json = sp.getString("w$appWidgetId", null)
            if (json != null) {
                onUpdate(context, views, appWidgetId, JSONObject(json))
                appWidgetManager.updateAppWidget(appWidgetId, views)
            }
        }
    }

    abstract fun onUpdate(context: Context, views: RemoteViews, appWidgetId: Int, model: JSONObject)

    abstract fun getRemoteViewsLayout(): Int

}