package webcase.com.ua.home_widget_plugin

import android.app.Activity
import android.appwidget.AppWidgetManager
import android.content.*
import androidx.core.app.ActivityCompat.startActivityForResult
import io.flutter.embedding.engine.plugins.FlutterPlugin
import io.flutter.embedding.engine.plugins.activity.ActivityAware
import io.flutter.embedding.engine.plugins.activity.ActivityPluginBinding
import io.flutter.plugin.common.*
import org.json.JSONArray
import webcase.com.ua.home_widget_plugin.HomeWidgetLaunchIntent.HOME_WIDGET_LAUNCH_ACTION


class HomeWidgetPlugin : FlutterPlugin, MethodChannel.MethodCallHandler, ActivityAware, PluginRegistry.NewIntentListener, EventChannel.StreamHandler {

    private lateinit var channel: MethodChannel
    private lateinit var eventChannel: EventChannel
    private lateinit var context: Context
    private var activity: Activity? = null
    private var receiver: BroadcastReceiver? = null

    override fun onAttachedToEngine(flutterPluginBinding: FlutterPlugin.FlutterPluginBinding) {
        setupChannel(flutterPluginBinding.binaryMessenger, flutterPluginBinding.applicationContext)
    }

    private fun setupChannel(messenger: BinaryMessenger, context: Context) {
        channel = MethodChannel(messenger, "home_widget_plugin")
        channel.setMethodCallHandler(this)

        eventChannel = EventChannel(messenger, "home_widget_plugin/updates")
        eventChannel.setStreamHandler(this)

        this.context = context
    }

    override fun onMethodCall(call: MethodCall, result: MethodChannel.Result) {
        when (call.method) {
            "setAppGroupId" -> {
                result.success(true)
            }
            "initiallyLaunchedFromHomeWidget" -> {
                return if (activity?.intent?.action?.equals(HOME_WIDGET_LAUNCH_ACTION) == true) {
                    result.success(activity?.intent?.data?.toString() ?: true)
                } else {
                    result.success(null)
                }
            }
            "registerBackgroundCallback" -> {
                val dispatcher = (call.arguments as Iterable<*>).toList()[0] as Long
                val callback = (call.arguments as Iterable<*>).toList()[1] as Long
                saveCallbackHandle(context, dispatcher, callback)
                return result.success(true)
            }
            "onRequestID" -> {
                if (activity?.intent?.action != AppWidgetManager.ACTION_APPWIDGET_CONFIGURE) {
                    return result.error("400", "Not ACTION_APPWIDGET_CONFIGURE", null)
                }
                val appWidgetID = activity?.intent?.extras?.getInt(AppWidgetManager.EXTRA_APPWIDGET_ID, AppWidgetManager.INVALID_APPWIDGET_ID)
                        ?: AppWidgetManager.INVALID_APPWIDGET_ID

                if (appWidgetID == AppWidgetManager.INVALID_APPWIDGET_ID) activity?.finish()
                result.success(appWidgetID)
            }
            "onAddWidget" -> {
                val pickIntent = Intent(AppWidgetManager.ACTION_APPWIDGET_PICK)
                pickIntent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, 1)
                activity?.startActivityForResult(pickIntent, 15000)
                return result.success(null)
            }
            "update" -> {
                val className = call.argument<String>("android")
                try {
                    val javaClass = Class.forName("${context.packageName}.${className}")
                    val intent = Intent(context, javaClass)
                    intent.action = AppWidgetManager.ACTION_APPWIDGET_UPDATE
                    val ids: IntArray = AppWidgetManager.getInstance(context.applicationContext).getAppWidgetIds(ComponentName(context, javaClass))
                    intent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS, ids)
                    context.sendBroadcast(intent)
                    result.success(true)
                } catch (classException: ClassNotFoundException) {
                    result.error("-3", "No Widget found with Name $className. Argument 'name' must be the same as your AppWidgetProvider you wish to update", classException)
                }
            }
            "saveValue" -> {
                try {
                    val key = call.argument<String>("key")!!
                    val value = call.argument<String>("value")!!
                    getData(context).edit().apply {
                        putString(key, value)
                        apply()
                    }
                    result.success(true)
                } catch (e: RuntimeException) {
                    result.error("-4", e.message, e)
                }
            }
            "getValue" -> {
                try {
                    val key = call.argument<String>("key")!!
                    result.success(getData(context).getString(key, null))
                } catch (e: RuntimeException) {
                    result.error("-7", e.message, e)
                }
            }
            "completeConfiguration" -> {
                try {
                    val appWidgetID = call.argument<Int>("widget_id") ?: 0
                    if (appWidgetID > 0) {
                        val resultValue = Intent().also {
                            it.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, appWidgetID)
                        }
                        activity!!.setResult(Activity.RESULT_OK, resultValue)
                    }
                    activity!!.finish()
                    result.success(true)
                } catch (e: RuntimeException) {
                    result.error("-5", e.message, e)
                }
            }
            else -> {
                result.notImplemented()
            }
        }
    }

    override fun onAttachedToActivity(binding: ActivityPluginBinding) {
        activity = binding.activity
        binding.addOnNewIntentListener(this)
    }

    override fun onDetachedFromActivityForConfigChanges() {
        unregisterReceiver()
        activity = null
    }

    override fun onReattachedToActivityForConfigChanges(binding: ActivityPluginBinding) {
        activity = binding.activity
        binding.addOnNewIntentListener(this)
    }

    override fun onDetachedFromActivity() {
        unregisterReceiver()
        activity = null
    }

    override fun onDetachedFromEngine(binding: FlutterPlugin.FlutterPluginBinding) {
        channel.setMethodCallHandler(null)
    }

  private fun createReceiver(events: EventChannel.EventSink?): BroadcastReceiver {
    return object : BroadcastReceiver() {
      override fun onReceive(context: Context?, intent: Intent?) {
        if (intent?.action.equals(HOME_WIDGET_LAUNCH_ACTION)) {
          events?.success(intent?.data?.toString() ?: true)
        }
      }
    }
  }

    private fun unregisterReceiver() {
        try {
            if (receiver != null) {
                context.unregisterReceiver(receiver)
            }
        } catch (e: IllegalArgumentException) {
            // Receiver not registered
        }
    }

    override fun onNewIntent(intent: Intent): Boolean {
        if (intent?.action == HOME_WIDGET_LAUNCH_ACTION) {
            receiver?.onReceive(context, intent)
            return receiver != null
        }
        return false
    }

    override fun onListen(arguments: Any?, events: EventChannel.EventSink?) {
        receiver = createReceiver(events)
    }

    override fun onCancel(arguments: Any?) {
        receiver = null
    }

    companion object {
        private const val PREFERENCES = "widgets"

        private const val INTERNAL_PREFERENCES = "InternalHomeWidgetPreferences"
        private const val CALLBACK_DISPATCHER_HANDLE = "callbackDispatcherHandle"
        private const val CALLBACK_HANDLE = "callbackHandle"

        private fun saveCallbackHandle(context: Context, dispatcher: Long, handle: Long) {
            context.getSharedPreferences(INTERNAL_PREFERENCES, Context.MODE_PRIVATE)
                    .edit()
                    .putLong(CALLBACK_DISPATCHER_HANDLE, dispatcher)
                    .putLong(CALLBACK_HANDLE, handle)
                    .apply()
        }

        fun getDispatcherHandle(context: Context): Long =
                context.getSharedPreferences(INTERNAL_PREFERENCES, Context.MODE_PRIVATE).getLong(CALLBACK_DISPATCHER_HANDLE, 0)

        fun getHandle(context: Context): Long =
                context.getSharedPreferences(INTERNAL_PREFERENCES, Context.MODE_PRIVATE).getLong(CALLBACK_HANDLE, 0)

        fun getData(context: Context): SharedPreferences = context.getSharedPreferences(PREFERENCES, Context.MODE_PRIVATE)
    }
}
