
import 'dart:async';

import 'package:flutter/services.dart';

class HomeWidgetPlugin {
  static const MethodChannel _channel =
      const MethodChannel('home_widget_plugin');
  static const EventChannel _eventChannel = EventChannel('home_widget_plugin/updates');

  static Future<String> get platformVersion async {
    final String version = await _channel.invokeMethod('getPlatformVersion');
    return version;
  }

  static Future<int?> get widgetID async {
    return await _channel.invokeMethod<int>("onRequestID");
  }

  static Future<void> completeConfiguration(int widgetID) async {
    return await _channel.invokeMethod('completeConfiguration', {'widget_id': widgetID});
  }

  static Future<void> saveValue(String key, String value) async {
    return await _channel.invokeMethod('saveValue', {'key': key, 'value': value});
  }

  static Future<String?> getValue(String key) async {
    return await _channel.invokeMethod<String>('getValue', {'key': key});
  }

  static Future<void> addWidget() async {
    return await _channel.invokeMethod("onAddWidget");
  }

  static Future<void> update(String android, String ios) async {
    return await _channel.invokeMethod('update', {'android': android, 'ios': ios});
  }

  static Future<Uri?> initiallyLaunchedFromHomeWidget() {
    return _channel
        .invokeMethod<String>('initiallyLaunchedFromHomeWidget')
        .then(_handleReceivedData);
  }

  static Future<bool?> setAppGroupId(String groupId) {
    return _channel.invokeMethod('setAppGroupId', {'groupId': groupId});
  }

  static Stream<Uri?> get widgetClicked {
    return _eventChannel
        .receiveBroadcastStream()
        .map<Uri?>(_handleReceivedData);
  }

  static Uri? _handleReceivedData(dynamic? value) {
    if (value != null) {
      if (value is String) {
        try {
          return Uri.parse(value);
        } on FormatException {
          print('Received Data($value) is not parsebale into an Uri');
        }
      }
      return Uri();
    } else {
      return null;
    }
  }

}
